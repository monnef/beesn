# 🚚🐝 Beesn

[![pipeline status](https://gitlab.com/monnef/beesn/badges/master/pipeline.svg?job=test)](https://gitlab.com/monnef/beesn/commits/master)

CLI utility for converting between JSON and BSON formats

## Usage

Arguments:
```
-x                       enables JSON -> BSON (default is BSON -> JSON)
-i FILE, --input FILE    name of input file, "-" for STDIN (default: "-")
-o FILE, --output FILE   name of output file, "-" for STDOUT (default: "-")
--stream                 reading a file containing multiple items, wrapping array is added
-w, --overwrite          allow overwriting output file
-d, --debug              debug output
-v, --verbose            print more info
-h, --help               print help text
```

### Examples

Read BSON, convert to JSON and print result:
```sh
$ beesn -i test-data/01.bson
{
  "a": "string",
  "b": [
    true,
    0
  ]
}
```

Read JSON, convert to BSON and save to file:
```sh
$ beesn -x -i test-data/01.json -o my.bson
$ beesn -i my.bson # print fresh BSON file
{
  "a": "string",
  "b": [
    true,
    0
  ]
}
```

## Dependencies
* [NodeJS](https://nodejs.org) (at least v8)

## Installation
`npm` alone had some issues when installing package globally.
I recommend using `yarn`.

```sh
$ sudo npm i -g yarn
$ sudo yarn global add beesn
```

## Stream mode
Can be used to read MongoDB dumps.
Be warned - there is only limited error detection and error reporting.

```sh
$ beesn --stream -i test-data/02-mongo.bson
```

## License
### MIT
