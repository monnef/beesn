import * as yargs from 'yargs';
import {Arguments} from 'yargs';
import * as fs from 'fs';
import * as BSON from 'bson-ext';
import {Option, Vector} from 'prelude.ts';

type Options = {
  verbose: boolean,
  showHelp: boolean,
  showVersion: boolean,
  debug: boolean,
  fileIn: string,
  fileOut: string,
  reverse: boolean,
  overwrite: boolean,
  streamMode: boolean
}

type DebugLog = (...args: any[]) => void;

const padEnd = (x: string, len: number, pad: string): string => (<any>x).padEnd(len, pad);

const zeroCharacter = String.fromCharCode(0);

const printVersion = () => {
  console.log(`🚚🐝 beesn %VERSION% written by monnef`);
};

const helpData: [string, string][] = [
  ['-x', 'enables JSON -> BSON (default is BSON -> JSON)'],
  ['-i FILE, --input FILE', 'name of input file, "-" for STDIN (default: "-")'],
  ['-o FILE, --output FILE', 'name of output file, "-" for STDOUT (default: "-")'],
  ['--stream', 'reading a file containing multiple items, wrapping array is added'],
  ['-w, --overwrite', 'allow overwriting output file'],
  ['-d, --debug', 'debug output'],
  ['-v, --verbose', 'print more info'],
  ['-h, --help', 'print help text'],
];
const printHelp = () =>
  Option.of(
    Vector.of<[string, string]>(['', ''], ...helpData)
      .map(([a, b]) => padEnd(a, 25, ' ') + b)
      .mkString('\n'),
  ).map(x => console.log(x));

const parseOptions = (args: Arguments): Options => {
  return {
    verbose: Boolean(args.v || args.verbose),
    showHelp: Boolean(args.h || args.help),
    showVersion: Boolean(args.V || args.version),
    debug: Boolean(args.d || args.debug),
    fileIn: String(args.i || args.input || '-'),
    fileOut: String(args.o || args.output || '-'),
    reverse: Boolean(args.x),
    overwrite: Boolean(args.w || args.overwrite),
    streamMode: Boolean(args.stream),
  };
};

const logDebug = (enabled: boolean) => (...args: any[]): void => {
  if (enabled) {
    console.log('D: ', ...args);
  }
};

const readInput = (fileName: string): string | Buffer => {
  return fs.readFileSync(fileName === '-' ? '/dev/stdin' : fileName);
};

interface DeserializeOneStreamItemResult {
  data: Option<any>;
  nextIndex: number;
}

const deserializeOneStreamItem = (bson: any, input: string | Buffer, index: number): DeserializeOneStreamItemResult => {
  let result = Option.none();
  let nextIndex = -1;
  try {
    const outArr: any[] = [];
    nextIndex = bson.deserializeStream(input, index, 1, outArr, 0, {});
    result = Option.some(outArr[0]);
  } catch (e) { }
  return {
    data: result,
    nextIndex,
  };
};

const deserializeStreamLoop = (input: string | Buffer, bson: any, res: any[], index: number): any[] => {
  const itemRes = deserializeOneStreamItem(bson, input, index);
  if (itemRes.data.isNone()) { return res; }
  return deserializeStreamLoop(input, bson, [...res, itemRes.data.getOrThrow()], itemRes.nextIndex);
};

const deserializeStream = (input: string | Buffer, bson: any): string => {
  return JSON.stringify(deserializeStreamLoop(input, bson, [], 0), null, 2);
};

const processData = (input: string | Buffer, bson: any, opts: Options): string => {
  const bsonDeserialization = !opts.reverse;
  if (bsonDeserialization) {
    if (opts.streamMode) {
      return deserializeStream(input, bson);
    } else {
      return JSON.stringify(bson.deserialize(input), null, 2);
    }
  } else {
    if (opts.streamMode) {
      throw new Error('not supported');
    } else {
      return bson.serialize(JSON.parse(input.toString()));
    }
  }
};

const writeOutput = (output: string | Buffer, fileName: string, overwrite: boolean): void => {
  if (fileName === '-') {
    fs.writeSync(1, output as NodeJS.ArrayBufferView);
  } else {
    fs.writeFileSync(fileName, output, {flag: overwrite ? 'w' : 'wx'});
  }
};

const main = (args: Arguments) => {
  const bson = new BSON([
    BSON.Binary, BSON.Code, BSON.DBRef, BSON.Decimal128, BSON.Double, BSON.Int32, BSON.Long, BSON.Map, BSON.MaxKey,
    BSON.MinKey, BSON.ObjectId, BSON.BSONRegExp, BSON.Symbol, BSON.Timestamp,
  ]);
  const opts = parseOptions(args);
  const d: DebugLog = logDebug(opts.debug);
  d(args);
  d(opts);

  if (opts.showHelp) {
    d('printing help');
    printVersion();
    printHelp();
  } else if (opts.showVersion) {
    d('printing version');
    printVersion();
  } else {
    d('reading input...');
    const inputData = readInput(opts.fileIn);
    d('processing data... input = ', inputData);
    const processedData = processData(inputData, bson, opts);
    d('writing data...');
    writeOutput(processedData, opts.fileOut, opts.overwrite);
  }
  d('done');
};

main(<Arguments>(<any>yargs).argv);
