import { expect } from 'chai';
import * as tmp from 'tmp';
import { exec, ExecOutputReturnValue } from 'shelljs';
import * as BSON from "bson-ext";
import * as fs from 'fs';

const bson = new BSON([
  BSON.Binary, BSON.Code, BSON.DBRef, BSON.Decimal128, BSON.Double, BSON.Int32, BSON.Long, BSON.Map, BSON.MaxKey,
  BSON.MinKey, BSON.ObjectId, BSON.BSONRegExp, BSON.Symbol, BSON.Timestamp
]);

const runCommandRaw = (cmd: string): ExecOutputReturnValue => {
  return <ExecOutputReturnValue>exec(cmd, {silent: true});
};

const runCommandExpectSuccess = (cmd: string): string => {
  const res = runCommandRaw(cmd);
  expect(res.code).to.eq(0, `Command "${cmd}" failed with ${res.code}: ${res.stderr}`);
  return res.stdout;
};

const appCmd = 'node dist/index.js';

const runApp = (args: string): string => {
  return runCommandExpectSuccess(`${appCmd} ${args}`);
};

const runAppRaw = (args: string): ExecOutputReturnValue => {
  return runCommandRaw(`${appCmd} ${args}`);
};

const j01name = 'test-data/01.json';
const b01name = 'test-data/01.bson';
const b02name = 'test-data/02-mongo.bson';
const read01json = () => JSON.parse(fs.readFileSync(j01name).toString());
const end = String.fromCharCode(0);

describe('beesn', () => {
  it('prints version', () => {
    const out = runApp('-V');
    let lines = out.split('\n');
    expect(lines).to.be.of.length(2);
    expect(out).to.contain('beesn');
    expect(out).to.contain('monnef');
  });

  it('prints help with version', () => {
    const out = runApp('-h');
    let lines = out.split('\n');
    expect(out).to.contain('beesn');
    expect(out).to.contain('--input');
    expect(lines).to.have.length(10 + 1);
  });

  it(`doesn't crash on reading file JSON`, () => {
    runApp(`-w -x -i ${j01name} -o /dev/null`);
  });

  it(`doesn't crash on reading file BSON`, () => {
    runApp(`-w -i ${b01name} -o /dev/null`);
  });

  it(`doesn't overwrite file without -x flag`, () => {
    const tmpFile = tmp.fileSync();
    const res = runAppRaw(`-i ${b01name} -o ${tmpFile.name} 2>&1`);
    expect(res.code).to.not.eq(0);
    expect(res.stdout).to.contain('EEXIST');
  });

  it(`file JSON -> stdout BSON`, () => {
    const converted = runApp(`-x -i ${j01name}`);
    const jsonFromBson = bson.deserialize(converted + end);
    expect(jsonFromBson).to.eql(read01json());
  });

  it(`file BSON -> stdout JSON`, () => {
    const converted = runApp(`-i ${b01name}`);
    expect(JSON.parse(converted)).to.eql(read01json());
  });

  it('files: JSON -> BSON -> JSON', () => {
    const bsonFile = tmp.fileSync();
    runApp(`-w -x -i ${j01name} -o ${bsonFile.name}`);

    const jsonFile = tmp.fileSync();
    runApp(`-w -i ${bsonFile.name} -o ${jsonFile.name}`);

    const roundJson = JSON.parse(fs.readFileSync(jsonFile.name).toString());

    expect(roundJson).to.eql(read01json());
  });

  it('pipe: JSON -> BSON -> JSON', () => {
    const resStr = runCommandExpectSuccess(`${appCmd} -x -i ${j01name} | ${appCmd}`);
    const res = JSON.parse(resStr);
    expect(res).to.eql(read01json());
  });

  it('MongoDB dump file from mongodump command: BSON -> JSON', () => {
    const resStr = runApp(`--stream -i ${b02name}`);
    const res = JSON.parse(resStr);
    expect(res).to.be.an('array');
    expect(res).to.be.of.length(6);
  });
});
