const fs = require('fs');
const path = require('path');

const name = path.join(__dirname, '../dist/index.js');
fs.chmodSync(name, '755');
