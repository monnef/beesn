"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var yargs = require("yargs");
var fs = require("fs");
var BSON = require("bson-ext");
var prelude_ts_1 = require("prelude.ts");
var padEnd = function (x, len, pad) { return x.padEnd(len, pad); };
var zeroCharacter = String.fromCharCode(0);
var printVersion = function () {
    console.log("\uD83D\uDE9A\uD83D\uDC1D beesn 0.0.5 written by monnef");
};
var helpData = [
    ['-x', 'enables JSON -> BSON (default is BSON -> JSON)'],
    ['-i FILE, --input FILE', 'name of input file, "-" for STDIN (default: "-")'],
    ['-o FILE, --output FILE', 'name of output file, "-" for STDOUT (default: "-")'],
    ['--stream', 'reading a file containing multiple items, wrapping array is added'],
    ['-w, --overwrite', 'allow overwriting output file'],
    ['-d, --debug', 'debug output'],
    ['-v, --verbose', 'print more info'],
    ['-h, --help', 'print help text'],
];
var printHelp = function () {
    return prelude_ts_1.Option.of(prelude_ts_1.Vector.of.apply(prelude_ts_1.Vector, __spreadArrays([['', '']], helpData)).map(function (_a) {
        var a = _a[0], b = _a[1];
        return padEnd(a, 25, ' ') + b;
    })
        .mkString('\n')).map(function (x) { return console.log(x); });
};
var parseOptions = function (args) {
    return {
        verbose: Boolean(args.v || args.verbose),
        showHelp: Boolean(args.h || args.help),
        showVersion: Boolean(args.V || args.version),
        debug: Boolean(args.d || args.debug),
        fileIn: String(args.i || args.input || '-'),
        fileOut: String(args.o || args.output || '-'),
        reverse: Boolean(args.x),
        overwrite: Boolean(args.w || args.overwrite),
        streamMode: Boolean(args.stream),
    };
};
var logDebug = function (enabled) { return function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    if (enabled) {
        console.log.apply(console, __spreadArrays(['D: '], args));
    }
}; };
var readInput = function (fileName) {
    return fs.readFileSync(fileName === '-' ? '/dev/stdin' : fileName);
};
var deserializeOneStreamItem = function (bson, input, index) {
    var result = prelude_ts_1.Option.none();
    var nextIndex = -1;
    try {
        var outArr = [];
        nextIndex = bson.deserializeStream(input, index, 1, outArr, 0, {});
        result = prelude_ts_1.Option.some(outArr[0]);
    }
    catch (e) { }
    return {
        data: result,
        nextIndex: nextIndex,
    };
};
var deserializeStreamLoop = function (input, bson, res, index) {
    var itemRes = deserializeOneStreamItem(bson, input, index);
    if (itemRes.data.isNone()) {
        return res;
    }
    return deserializeStreamLoop(input, bson, __spreadArrays(res, [itemRes.data.getOrThrow()]), itemRes.nextIndex);
};
var deserializeStream = function (input, bson) {
    return JSON.stringify(deserializeStreamLoop(input, bson, [], 0), null, 2);
};
var processData = function (input, bson, opts) {
    var bsonDeserialization = !opts.reverse;
    if (bsonDeserialization) {
        if (opts.streamMode) {
            return deserializeStream(input, bson);
        }
        else {
            return JSON.stringify(bson.deserialize(input), null, 2);
        }
    }
    else {
        if (opts.streamMode) {
            throw new Error('not supported');
        }
        else {
            return bson.serialize(JSON.parse(input.toString()));
        }
    }
};
var writeOutput = function (output, fileName, overwrite) {
    if (fileName === '-') {
        fs.writeSync(1, output);
    }
    else {
        fs.writeFileSync(fileName, output, { flag: overwrite ? 'w' : 'wx' });
    }
};
var main = function (args) {
    var bson = new BSON([
        BSON.Binary, BSON.Code, BSON.DBRef, BSON.Decimal128, BSON.Double, BSON.Int32, BSON.Long, BSON.Map, BSON.MaxKey,
        BSON.MinKey, BSON.ObjectId, BSON.BSONRegExp, BSON.Symbol, BSON.Timestamp,
    ]);
    var opts = parseOptions(args);
    var d = logDebug(opts.debug);
    d(args);
    d(opts);
    if (opts.showHelp) {
        d('printing help');
        printVersion();
        printHelp();
    }
    else if (opts.showVersion) {
        d('printing version');
        printVersion();
    }
    else {
        d('reading input...');
        var inputData = readInput(opts.fileIn);
        d('processing data... input = ', inputData);
        var processedData = processData(inputData, bson, opts);
        d('writing data...');
        writeOutput(processedData, opts.fileOut, opts.overwrite);
    }
    d('done');
};
main(yargs.argv);
