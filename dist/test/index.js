"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var tmp = require("tmp");
var shelljs_1 = require("shelljs");
var BSON = require("bson-ext");
var fs = require("fs");
var bson = new BSON([
    BSON.Binary, BSON.Code, BSON.DBRef, BSON.Decimal128, BSON.Double, BSON.Int32, BSON.Long, BSON.Map, BSON.MaxKey,
    BSON.MinKey, BSON.ObjectId, BSON.BSONRegExp, BSON.Symbol, BSON.Timestamp
]);
var runCommandRaw = function (cmd) {
    return shelljs_1.exec(cmd, { silent: true });
};
var runCommandExpectSuccess = function (cmd) {
    var res = runCommandRaw(cmd);
    chai_1.expect(res.code).to.eq(0, "Command \"" + cmd + "\" failed with " + res.code + ": " + res.stderr);
    return res.stdout;
};
var appCmd = 'node dist/index.js';
var runApp = function (args) {
    return runCommandExpectSuccess(appCmd + " " + args);
};
var runAppRaw = function (args) {
    return runCommandRaw(appCmd + " " + args);
};
var j01name = 'test-data/01.json';
var b01name = 'test-data/01.bson';
var b02name = 'test-data/02-mongo.bson';
var read01json = function () { return JSON.parse(fs.readFileSync(j01name).toString()); };
var end = String.fromCharCode(0);
describe('beesn', function () {
    it('prints version', function () {
        var out = runApp('-V');
        var lines = out.split('\n');
        chai_1.expect(lines).to.be.of.length(2);
        chai_1.expect(out).to.contain('beesn');
        chai_1.expect(out).to.contain('monnef');
    });
    it('prints help with version', function () {
        var out = runApp('-h');
        var lines = out.split('\n');
        chai_1.expect(out).to.contain('beesn');
        chai_1.expect(out).to.contain('--input');
        chai_1.expect(lines).to.have.length(10 + 1);
    });
    it("doesn't crash on reading file JSON", function () {
        runApp("-w -x -i " + j01name + " -o /dev/null");
    });
    it("doesn't crash on reading file BSON", function () {
        runApp("-w -i " + b01name + " -o /dev/null");
    });
    it("doesn't overwrite file without -x flag", function () {
        var tmpFile = tmp.fileSync();
        var res = runAppRaw("-i " + b01name + " -o " + tmpFile.name + " 2>&1");
        chai_1.expect(res.code).to.not.eq(0);
        chai_1.expect(res.stdout).to.contain('EEXIST');
    });
    it("file JSON -> stdout BSON", function () {
        var converted = runApp("-x -i " + j01name);
        var jsonFromBson = bson.deserialize(converted + end);
        chai_1.expect(jsonFromBson).to.eql(read01json());
    });
    it("file BSON -> stdout JSON", function () {
        var converted = runApp("-i " + b01name);
        chai_1.expect(JSON.parse(converted)).to.eql(read01json());
    });
    it('files: JSON -> BSON -> JSON', function () {
        var bsonFile = tmp.fileSync();
        runApp("-w -x -i " + j01name + " -o " + bsonFile.name);
        var jsonFile = tmp.fileSync();
        runApp("-w -i " + bsonFile.name + " -o " + jsonFile.name);
        var roundJson = JSON.parse(fs.readFileSync(jsonFile.name).toString());
        chai_1.expect(roundJson).to.eql(read01json());
    });
    it('pipe: JSON -> BSON -> JSON', function () {
        var resStr = runCommandExpectSuccess(appCmd + " -x -i " + j01name + " | " + appCmd);
        var res = JSON.parse(resStr);
        chai_1.expect(res).to.eql(read01json());
    });
    it('MongoDB dump file from mongodump command: BSON -> JSON', function () {
        var resStr = runApp("--stream -i " + b02name);
        var res = JSON.parse(resStr);
        chai_1.expect(res).to.be.an('array');
        chai_1.expect(res).to.be.of.length(6);
    });
});
